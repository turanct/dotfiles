" Wrap selected text in parenthesis, brackets, quotes
vnoremap <Leader>(  <ESC>`>a)<ESC>`<i(<ESC>
vnoremap <Leader>)  <ESC>`>a)<ESC>`<i(<ESC>
vnoremap <Leader>{  <ESC>`>a}<ESC>`<i{<ESC>
vnoremap <Leader>}  <ESC>`>a}<ESC>`<i{<ESC>
vnoremap <Leader>[  <ESC>`>a]<ESC>`<i[<ESC>
vnoremap <Leader>]  <ESC>`>a]<ESC>`<i[<ESC>
vnoremap <Leader>"  <ESC>`>a"<ESC>`<i"<ESC>
vnoremap <Leader>'  <ESC>`>a'<ESC>`<i'<ESC>
vnoremap ` <ESC>`>a`<ESC>`<i`<ESC>
vnoremap <Leader>*  <ESC>`>o*/<ESC>`<O/*<ESC>gv

" Autocomplete closing brackets
inoremap (  ()<Esc>i
inoremap {  {}<Esc>i
inoremap [  []<Esc>i

" Skip to next character when typing closing bracket before a closing bracket
inoremap <expr> ) strpart(getline('.'), col('.')-1, 1) == ")" ? "\<Right>" : ")"
inoremap <expr> } strpart(getline('.'), col('.')-1, 1) == "}" ? "\<Right>" : "}"
inoremap <expr> ] strpart(getline('.'), col('.')-1, 1) == "]" ? "\<Right>" : "]"
inoremap <expr> " strpart(getline('.'), col('.')-1, 1) == "\"" ? "\<Right>" : "\"\"\<Esc>i"
inoremap <expr> ' strpart(getline('.'), col('.')-1, 1) == "'" ? "\<Right>" : "''\<Esc>i"
inoremap <expr> ` strpart(getline('.'), col('.')-1, 1) == "`" ? "\<Right>" : "``\<Esc>i"
