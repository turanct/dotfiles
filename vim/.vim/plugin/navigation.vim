" Less finger wrecking window navigation.
" nnoremap <c-j> <c-w>j
" nnoremap <c-k> <c-w>k
nnoremap <c-h> <c-w>h
nnoremap <c-l> <c-w>l

" Half page down/up
nnoremap <c-j> <c-d>
nnoremap <c-k> <c-u>

" Hop from method to method
nmap <Leader>f ]]
nmap <Leader>g [[

" Hop from gitgutter hunk to hunk
nmap <Leader>v :GitGutterNextHunk<CR>
nmap <Leader>b :GitGutterPrevHunk<CR>

" Hop from buffer to buffer
nmap gb :bnext<CR>
nmap gB :bprevious<CR>
