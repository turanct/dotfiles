" Ack -> Ag
if executable('ag')
    let g:ackprg = 'ag --vimgrep'
endif

" CtrlP
noremap <Leader><Space> :CtrlPTag<CR>
let g:ctrlp_cmd = 'CtrlPMixed'
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" Finding files with :find
set path+=**
