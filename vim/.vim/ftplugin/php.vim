" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

" PHP linter
noremap <Leader>p :echom system("php -l ".expand('%'))<CR>

" Ctags completion
set tags=./tags;/       " Ctags, look in current dir and up
set completeopt=longest,menuone
set omnifunc=syntaxcomplete#Complete
noremap <Leader>t :echom system("/usr/local/bin/ctags -R . 2>/dev/null &")<CR>
noremap <Leader>a <c-w><c-]><c-w>T
inoremap <c-b> <c-x><c-o>
inoremap <c-l> <c-x><c-l>

" PHP refactoring browser
let g:php_refactor_command='/usr/local/bin/refactor'

" Ale
let g:ale_linters = {'php': ['php', 'phpcs', 'phpmd']}
let g:ale_php_phpcs_standard = 'psr2'
let g:ale_php_phpmd_ruleset  = 'cleancode,codesize,controversial,design,naming,unusedcode'
let g:ale_completion_enabled = 0

" PHP-CS-Fixer plugin
let g:php_cs_fixer_rules = '@PSR2'

augroup php
    " Remove all php autocommands
    autocmd!

    autocmd BufWritePost *.php silent! :echom system("/usr/local/bin/ctags -R . &2>/dev/null")
    autocmd BufWritePost *.php silent! call PhpCsFixerFixFile()
augroup END
