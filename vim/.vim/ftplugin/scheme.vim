" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

augroup scheme
    " Remove all scheme autocommands
    autocmd!

    " Run the chicken compiler after saving a file
    autocmd BufWritePost {*.scm} :! csc % && ./%:r
augroup END
