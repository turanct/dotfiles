" Only do this when not done yet for this buffer
if exists("b:did_ftplugin")
  finish
endif
let b:did_ftplugin = 1

" Ale
let g:ale_linters = {'haskell': ['hie']}
let g:ale_completion_enabled = 0

" Syntax highlighting
let g:haskell_indent_disable = 1
let g:haskell_classic_highlighting = 1

" Compiler
compiler hspec

augroup haskell
    " Remove all haskell autocommands
    autocmd!

    autocmd CompleteDone * silent! pclose
augroup END
