" Some basic settings
set hidden              " Hide buffers instead of closing them
set history=1000        " Keep 100 lines of history
set ruler               " Show the cursor position
filetype plugin indent on " use the file type plugins
set wildmenu            " Autocomplete menu options in statusline
let mapleader = ','     " Remap leader key to comma
set backspace=indent,eol,start " Make the backspace key work naturally

" File formatting
set fileformat=unix     " Always use unix fileformat.
set encoding=utf-8      " Force UTF-8 encoding.

" Visuals
syntax on               " Syntax highlighting
set number nu           " Line numbers
set cursorline          " Color the cursorline

" Colorscheme
set background=light
color PaperColor

" Indentation
set autoindent          " Auto indenting
set smartindent         " Smart indenting
set tabstop=4           " Tab width
set shiftwidth=4        " Shift width
set expandtab           " Use spaces instead of tabs

" Set visual characters
set listchars=tab:→\ ,extends:»,precedes:«,trail:▒,nbsp:·
set list
noremap <Leader>l :set list!<CR>

" Search
set ignorecase
set smartcase
set incsearch
set hlsearch

" Mouse support
set mouse=a
set ttymouse=xterm2

" Set statusline
set statusline=%<\ %n:%f\ %m%r%y%=%-35.(line:\ %l\ of\ %L,\ col:\ %c%V\ (%P)%)
set laststatus=2        " Always show statusline

" Use the compiler
noremap <leader>m :make<CR>
" Automatically open & close quickfix window
autocmd QuickFixCmdPost [^l]* nested cwindow

" Git leader hotkeys
nmap <Leader>gs :Gstatus<CR>
nmap <Leader>gc :Gcommit<CR>
nmap <Leader>gb :Gblame<CR>

" NERDTree leader hotkeys
nmap <Leader>nt :NERDTreeToggle<CR>
nmap <Leader>nf :NERDTreeFind<CR>

" Vim save session
noremap <Leader>s :mksession! session.vim<CR>

" Run a terminal command fast
noremap <Leader>c :! 

" Refer to the current file's dir with %%
cnoremap <expr> %% getcmdtype() == ':' ? expand('%:h').'/' : '%%'
