# Toon: Add Colors To CLI
export CLICOLOR=1

# Function to show the current git branch
function parse_git_branch {
	git branch --no-color 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/(\1)/'
}

# Git autocompletion
GIT_COMPLETION="$(brew --prefix)/etc/bash_completion.d/git-completion.bash"
if [ -f $GIT_COMPLETION ]; then
	source $GIT_COMPLETION
fi

# Pass autocompletion
PASS_COMPLETION="$(brew --prefix)/etc/bash_completion.d/pass"
if [ -f $PASS_COMPLETION ]; then
	source $PASS_COMPLETION
fi

# Toon: Colorful first CLI line
export PS1="\[\e[32;1m\]\w \[\e[31;1m\]\$(parse_git_branch)\n\[\e[36;1m\]What are your orders, master?\n$ \[\e[0m\]"

# Toon: My default editor
export EDITOR="vim"

# Colorized man pages
man() {
	env \
	LESS_TERMCAP_mb=$(printf "\e[1;31m") \
	LESS_TERMCAP_md=$(printf "\e[1;31m") \
	LESS_TERMCAP_me=$(printf "\e[0m") \
	LESS_TERMCAP_se=$(printf "\e[0m") \
	LESS_TERMCAP_so=$(printf "\e[1;44;33m") \
	LESS_TERMCAP_ue=$(printf "\e[0m") \
	LESS_TERMCAP_us=$(printf "\e[1;32m") \
	man "$@"
}

# Aliasses ------------------------------------------------------------------
# Toon: Magnificent GitLog
alias gitlog='git log --decorate --name-status'
alias gitlogshort='git log --pretty=format:"%h%x09%an%x09%ad%x09%s"'

# Toon: Sublime Text 2
alias subl='/Applications/Sublime\ Text\ 2.app/Contents/SharedSupport/bin/subl'

# Toon: Start Screensaver Alias
alias screensaver='/System/Library/Frameworks/ScreenSaver.framework/Resources/ScreenSaverEngine.app/Contents/MacOS/ScreenSaverEngine'

# Toon: Backup Muziek
alias backup-muziek='rsync -avzru Music/MUZIEK/ /Volumes/LACIE/Music/MUZIEK --exclude "/Applications/**" --exclude ".*" --exclude ".*/" --exclude ".*/**"'

# Toon: Backup LACIE -> R2D2
alias backup-schijf='rsync -avzru /Volumes/LACIE/ /Volumes/R2D2 --exclude "/Applications/**" --exclude ".*" --exclude ".*/" --exclude ".*/**"'

# Toon: Clockradio
alias clockradio='php /Applications/ClockRadio/ClockRadio.php'

# Toon: Purse
alias purse='php /Applications/PurseToons/Purse.php'

# VMWare Fusion Networking
alias vmware='sudo /Library/Application\ Support/VMware\ Fusion/boot.sh'

# Remove OSX Download protection for a file/directory
alias fixdownload='xattr -d -r com.apple.quarantine'

# Current iTunes Song
function currentsong {
	osascript -e 'tell application "iTunes" to set myTrack to artist of current track & " - " & name of current track'
}

# Show random replygif
function replygif {
	local randomgif=`curl --silent http://replygif.net/random | grep img | sed -e 's/^.*src="//g' -e 's/" alt.*$//g'`
	echo "$randomgif" | pbcopy
	open "$randomgif"
}

# Show all functions in a php file
function functions {
	cat "$@" | grep function | grep "(" | grep ")" | sed -e 's/^[[:space:]]*//'
}

# Start a server in the current directory
function server() {
	local port="${1:-8000}"
	open "http://localhost:${port}/"
	python -m SimpleHTTPServer "$port"
}

# JSON Pretty Print
alias jsonpp='python -m json.tool'

# Open vim on a tag search
alias vimtag='vim -t'

# php-cs-fixer oneliner
alias tooncsfix='php-cs-fixer fix . --dry-run --verbose --diff --level=psr2 --fixers=-braces,-indentation'
alias tooncsfixr='php-cs-fixer fix --dry-run --verbose --diff --level=psr2'

# Javascript interactive console
alias jsc='/System/Library/Frameworks/JavaScriptCore.framework/Versions/Current/Resources/jsc'

# Newsbeuter RSS Reeder in English
alias feeds="LANG=en_GB newsboat"

# Make bash work like vi
alias bashvim="set -o vi"

# Curl headers
alias headers="curl -I"

# Show a history of reboots
alias reboothistory="last reboot"

# Nice tree view of the current directory
alias lstree="tree -L 1 -a -F -C"

# Toon: Show Aliasses
alias aliasses='cat ~/.bashrc | grep "alias " | sed -e "s/alias //g" | sed -e "s/=.*//g" | sort'

# Toon: lint changed php files
alias lintphp='git diff --name-only HEAD | grep "\.php" | xargs -n 1 php -l | grep -v "No syntax errors"'

# Make /usr/local/bin appear before /usr/bin in path
export PATH="/Users/toon/.composer/vendor/bin:/Users/toon/.local/bin:/usr/local/bin:/usr/local/sbin:$PATH"

# Use z directory finder
Z_PATH="/usr/local/Cellar/z/1.9/etc/profile.d/z.sh"
if [ -f $Z_PATH ]; then
	source $Z_PATH
fi

export LC_ALL=en_US.UTF-8
