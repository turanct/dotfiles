Homedir Stow
===========================================

This is a repository which can be used with stow to re-create (parts of) my `HOME`
How does it work?


1. Clone this repo
-------------------------------------------
You should clone this repo in your home directory. You can rename it to be hidden (add a `.`) or rename it to whatever you like.


2. Symlink files to the appropriate place
-------------------------------------------

### 2.1 Manually
You can do this manually (not a good idea)

	ln -s {/path/to/the/file} {/path/for/the/link}


### 2.2 Stow
You should let GNU stow do the hard work.

#### 2.2.1 Install Stow
##### 2.2.1.1 On Mac OSX
Use Homebrew

	brew install stow

##### 2.2.1.2 On Debian based GNU/Linux
Use apt

	apt-get install stow

##### 2.2.1.3 On RedHat based GNU/Linux
Use yum

	yum install stow

#### 2.2.2 Use stow
##### 2.2.2.1 Navigate to the dotfiles repository

	cd dotfiles

##### 2.2.2.2 Stow the package

	stow {package}

##### 2.2.2.3 Unstow a package

	stow --delete {package}

##### 2.2.2.4 Show what would happen

	stow --no --verbose {package}


3. Profit
-------------------------------------------
Profit!
